
export class place{
    public city: string;
    public country: string;
    constructor(
        city: string, 
        country: string
    ) {
        this.city = city;
        this.country = country;
      }
}
export class  weatherinfo{
    public mainweatherinfo: mainweatherinfo;
    public weathertype: weathertype;
    public clouds: clouds;
    public wind: wind;
    public rain: rain;
    public snow: snow;
    public visability: number;
    public time: string;

  constructor(
    mainweatherinfo: mainweatherinfo, 
    weathertype: weathertype, 
    clouds: clouds, 
    wind: wind, 
    rain: rain, 
    snow: snow, 
    visability: number, 
    time: string
) {
    this.mainweatherinfo = mainweatherinfo
    this.weathertype = weathertype
    this.clouds = clouds
    this.wind = wind
    this.rain = rain
    this.snow = snow
    this.visability = visability
    this.time = time
  }

}
export class mainweatherinfo{
    public temp: number;
    public min_temp: number;
    public max_temp: number;
    public pressure: number;
    public humidity: number;

  constructor(
    temp: number, 
    min_temp: number, 
    max_temp: number, 
    pressure: number, 
    humidity: number
) {
    this.temp = temp
    this.min_temp = min_temp
    this.max_temp = max_temp
    this.pressure = pressure
    this.humidity = humidity
  }

};
export class weathertype{
    public name: string;
    public description: string;

  constructor(name: string, description: string) {
    this.name = name
    this.description = description
  }

}
export class clouds{
    public amount: number;

  constructor(amount: number) {
    this.amount = amount
  }

}
export class wind{
    public speed: number;
    public deg: number;

  constructor(speed: number, deg: number) {
    this.speed = speed
    this.deg = deg
  }

}
export class rain{
    public amount: number;

  constructor(amount: number) {
    this.amount = amount
  }

}
export class snow{
    public amount: number;

  constructor(amount: number) {
    this.amount = amount
  }

}
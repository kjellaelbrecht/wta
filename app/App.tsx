import { useEffect, useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Dimensions,ScrollView } from 'react-native';
import * as Location from 'expo-location';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph
} from 'react-native-chart-kit';
import * as types from './types';
import { DataTable } from 'react-native-paper';

const axios = require('axios').default;
const win = Dimensions.get('window')
export default function App() {
  const [place, setplace] = useState<types.place>();
  const [temp, settmp] = useState<Array<number>>([0,0,0,0,0,0,0,0]);
  const [mintemp, setmintmp] = useState<Array<number>>([0,0,0,0,0,0,0,0]);
  const [maxtemp, setmaxtmp] = useState<Array<number>>([0,0,0,0,0,0,0,0]);
  const [humi, sethumi] = useState<Array<number>>([0,0,0,0,0,0,0,0]);
  const [pressure, setpressure] = useState<Array<number>>([0,0,0,0,0,0,0,0,0]);
  const [windspeed, setwintspeed] = useState<Array<number>>([0,0,0,0,0,0,0,0,0]);
  const [winddirection, setwinddirection] = useState<Array<string>>(['N','N','N','N','N','N','N','N','N']);
  const [vis, setvis] = useState<Array<number>>([0,0,0,0,0,0,0,0,0]);
  const [rain, setrain] = useState<Array<number>>([0,0,0,0,0,0,0,0,0]);
  const [snow, setsnow] = useState<Array<number>>([0,0,0,0,0,0,0,0,0]);
  const [weather, setweather] = useState<Array<string>>(['','','','','','','','','']);
  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        console.log('Permission to access location was denied');
        return;
      }
      const convtowinddirection = function(inp:number):string{
        if(inp<315){
          if(inp<270){
            if(inp<225){
              if(inp<180){
                if(inp<135){
                  if(inp<90){
                    if(inp<45){
                      return "N"
                    }
                    return "NE"
                  }
                  return "E"
                }
                return "SE"
              }
              return "S"
            }
            return "SW"
          }
          return "W"
        }
        return "NW"
      }
      let location = await Location.getCurrentPositionAsync({});
      let latitude = location.coords.latitude;
      let longitude = location.coords.longitude;
      var options = {
        method: 'GET',
        url: 'https://community-open-weather-map.p.rapidapi.com/forecast',
        params: {lat: latitude, lon: longitude,cnt:9},
        headers: {
          'x-rapidapi-host': 'community-open-weather-map.p.rapidapi.com',
          'x-rapidapi-key': 'a3d3ec8bd9mshcc0ab1443eecc1bp196546jsn11be82f3a1d6'
        }
      };
      var g3 = 'ff';
      var resp = await axios.request(options);
      var tmp = resp.data;
      var weatherlist = Array<types.weatherinfo>();
      setplace(new types.place(tmp.city.name,tmp.city.country))
      tmp.list?.map(x =>{
        var maininfo = new types.mainweatherinfo(x.main.temp-272.15,x.main.temp_min-272.15,x.main.temp_max-272.15,x.main.pressure,x.main.humidity);
        var weather = new types.weathertype(x.weather[0].main,x.weather[0].description)
        var clouds = new types.clouds(x.clouds.all);
        var wind = new types.wind(x.wind.speed,x.wind.deg)
        var snow = new types.snow(0)
        if(x.snow != undefined){
          snow = new types.snow(Number(JSON.stringify(x.snow).substring(6).replace('}', ""))) 
        }
        var rain = new types.rain(0)
        if(x.rain != undefined){
          rain = new types.rain(Number(JSON.stringify(x.rain).substring(6).replace('}', "")))
        }
        var visibility = x.visibility;
        var time = x.dt_txt;
        var total = new types.weatherinfo(maininfo,weather,clouds,wind,rain,snow,visibility,time);
        weatherlist.push(total);
      })
      weatherlist
      settmp([weatherlist[0].mainweatherinfo.temp,
        weatherlist[1].mainweatherinfo.temp,
        weatherlist[2].mainweatherinfo.temp,
        weatherlist[3].mainweatherinfo.temp,
        weatherlist[4].mainweatherinfo.temp,
        weatherlist[5].mainweatherinfo.temp,
        weatherlist[6].mainweatherinfo.temp,
        weatherlist[7].mainweatherinfo.temp,
        weatherlist[8].mainweatherinfo.temp]);
      setmaxtmp([weatherlist[0].mainweatherinfo.max_temp,
        weatherlist[1].mainweatherinfo.max_temp,
        weatherlist[2].mainweatherinfo.max_temp,
        weatherlist[3].mainweatherinfo.max_temp,
        weatherlist[4].mainweatherinfo.max_temp,
        weatherlist[5].mainweatherinfo.max_temp,
        weatherlist[6].mainweatherinfo.max_temp,
        weatherlist[7].mainweatherinfo.max_temp,
        weatherlist[8].mainweatherinfo.max_temp]);
      setmintmp([weatherlist[0].mainweatherinfo.min_temp,
        weatherlist[1].mainweatherinfo.min_temp,
        weatherlist[2].mainweatherinfo.min_temp,
        weatherlist[3].mainweatherinfo.min_temp,
        weatherlist[4].mainweatherinfo.min_temp,
        weatherlist[5].mainweatherinfo.min_temp,
        weatherlist[6].mainweatherinfo.min_temp,
        weatherlist[7].mainweatherinfo.min_temp,
        weatherlist[8].mainweatherinfo.min_temp]);
      sethumi([weatherlist[0].mainweatherinfo.humidity,
        weatherlist[1].mainweatherinfo.humidity,
        weatherlist[2].mainweatherinfo.humidity,
        weatherlist[3].mainweatherinfo.humidity,
        weatherlist[4].mainweatherinfo.humidity,
        weatherlist[5].mainweatherinfo.humidity,
        weatherlist[6].mainweatherinfo.humidity,
        weatherlist[7].mainweatherinfo.humidity,
        weatherlist[8].mainweatherinfo.humidity])
      sethumi([weatherlist[0].mainweatherinfo.humidity,
        weatherlist[1].mainweatherinfo.humidity,
        weatherlist[2].mainweatherinfo.humidity,
        weatherlist[3].mainweatherinfo.humidity,
        weatherlist[4].mainweatherinfo.humidity,
        weatherlist[5].mainweatherinfo.humidity,
        weatherlist[6].mainweatherinfo.humidity,
        weatherlist[7].mainweatherinfo.humidity,
        weatherlist[8].mainweatherinfo.humidity])
      setpressure([weatherlist[0].mainweatherinfo.pressure,
        weatherlist[1].mainweatherinfo.pressure,
        weatherlist[2].mainweatherinfo.pressure,
        weatherlist[3].mainweatherinfo.pressure,
        weatherlist[4].mainweatherinfo.pressure,
        weatherlist[5].mainweatherinfo.pressure,
        weatherlist[6].mainweatherinfo.pressure,
        weatherlist[7].mainweatherinfo.pressure,
        weatherlist[8].mainweatherinfo.pressure])
        setweather([weatherlist[0].weathertype.description,
          weatherlist[1].weathertype.description,
          weatherlist[2].weathertype.description,
          weatherlist[3].weathertype.description,
          weatherlist[4].weathertype.description,
          weatherlist[5].weathertype.description,
          weatherlist[6].weathertype.description,
          weatherlist[7].weathertype.description,
          weatherlist[8].weathertype.description])
        setwintspeed([weatherlist[0].wind.speed,
          weatherlist[1].wind.speed,
          weatherlist[2].wind.speed,
          weatherlist[3].wind.speed,
          weatherlist[4].wind.speed,
          weatherlist[5].wind.speed,
          weatherlist[6].wind.speed,
          weatherlist[7].wind.speed,
          weatherlist[8].wind.speed])
          setrain([weatherlist[0].rain.amount,
            weatherlist[1].rain.amount,
            weatherlist[2].rain.amount,
            weatherlist[3].rain.amount,
            weatherlist[4].rain.amount,
            weatherlist[5].rain.amount,
            weatherlist[6].rain.amount,
            weatherlist[7].rain.amount,
            weatherlist[8].rain.amount])
            setsnow([weatherlist[0].snow.amount,
              weatherlist[1].snow.amount,
              weatherlist[2].snow.amount,
              weatherlist[3].snow.amount,
              weatherlist[4].snow.amount,
              weatherlist[5].snow.amount,
              weatherlist[6].snow.amount,
              weatherlist[7].snow.amount,
              weatherlist[8].snow.amount])
        setwinddirection([convtowinddirection(weatherlist[0].wind.deg),
        convtowinddirection(weatherlist[1].wind.deg),
        convtowinddirection(weatherlist[2].wind.deg),
        convtowinddirection(weatherlist[3].wind.deg),
        convtowinddirection(weatherlist[4].wind.deg),
        convtowinddirection(weatherlist[5].wind.deg),
        convtowinddirection(weatherlist[6].wind.deg),
        convtowinddirection(weatherlist[7].wind.deg),
        convtowinddirection(weatherlist[8].wind.deg)])
    })();
    
  }, []);

  return (
    
      <ScrollView>
        <View style={styles.container}>
      <Text style={styles.title}>{place?.city} {place?.country}</Text>
      <Text style={styles.title2}>temprature</Text>
      <LineChart
    data={{
      labels: ['+0', '+3', '+6', '+9', '+12', '+15','+18','+21','+24'],
      datasets: [
        {
        data: temp,
        color:(opacity = 1) => `rgba(0, 255, 0,${opacity})`
        },
        {
          data: mintemp,
          color:(opacity = 1) => `rgba(255, 0, 0,${opacity})`
        },
        {
          data: maxtemp,
          color:(opacity = 1) => `rgba(255, 0, 0,${opacity})`
        }
    ]
    }}
    width={Dimensions.get('window').width}
    height={220}
    chartConfig={{
      backgroundColor: '#e26a00',
      backgroundGradientFrom: '#fb8c00',
      backgroundGradientTo: '#ffa726',
      decimalPlaces: 2,
      color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
      style: {
        borderRadius: 16
      }
    }}
    fromZero={true}
    style={{
      marginVertical: 8,
      borderRadius: 16
    }}
  />
<Text style={styles.title2}>weather</Text>
<DataTable style={styles.table}>
<DataTable.Header>
          <DataTable.Title>timespan</DataTable.Title>
          <DataTable.Title>weather</DataTable.Title>         
</DataTable.Header>
<DataTable.Row>
          <DataTable.Cell>+0</DataTable.Cell>
          <DataTable.Cell>{weather[0]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+3</DataTable.Cell>
          <DataTable.Cell>{weather[1]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+6</DataTable.Cell>
          <DataTable.Cell>{weather[2]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+9</DataTable.Cell>
          <DataTable.Cell>{weather[3]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+12</DataTable.Cell>
          <DataTable.Cell>{weather[4]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+15</DataTable.Cell>
          <DataTable.Cell>{weather[5]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+18</DataTable.Cell>
          <DataTable.Cell>{weather[6]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+21</DataTable.Cell>
          <DataTable.Cell>{weather[7]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+24</DataTable.Cell>
          <DataTable.Cell>{weather[8]}</DataTable.Cell>
</DataTable.Row>
</DataTable>
<Text style={styles.title2}>wind</Text>
<Text style={styles.title3}>windspeed</Text>
<LineChart
    data={{
      labels: ['+0', '+3', '+6', '+9', '+12', '+15','+18','+21','+24'],
      datasets: [
        {
        data: windspeed,
        color:(opacity = 1) => `rgba(0, 255, 0,${opacity})`
        }
    ]
    }}
    width={Dimensions.get('window').width}
    height={220}
    chartConfig={{
      backgroundColor: '#e26a00',
      backgroundGradientFrom: '#fb8c00',
      backgroundGradientTo: '#ffa726',
      decimalPlaces: 2,
      color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
      style: {
        borderRadius: 16
      }
    }}
    
    style={{
      marginVertical: 8,
      borderRadius: 16
    }}
  />
  <Text style={styles.title3}>wind direction</Text>
  <DataTable style={styles.table}>
<DataTable.Header>
          <DataTable.Title>timespan</DataTable.Title>
          <DataTable.Title>wind direction</DataTable.Title>         
</DataTable.Header>
<DataTable.Row>
          <DataTable.Cell>+0</DataTable.Cell>
          <DataTable.Cell>{winddirection[0]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+3</DataTable.Cell>
          <DataTable.Cell>{winddirection[1]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+6</DataTable.Cell>
          <DataTable.Cell>{winddirection[2]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+9</DataTable.Cell>
          <DataTable.Cell>{winddirection[3]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+12</DataTable.Cell>
          <DataTable.Cell>{winddirection[4]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+15</DataTable.Cell>
          <DataTable.Cell>{winddirection[5]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+18</DataTable.Cell>
          <DataTable.Cell>{winddirection[6]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+21</DataTable.Cell>
          <DataTable.Cell>{winddirection[7]}</DataTable.Cell>
</DataTable.Row>
<DataTable.Row>
          <DataTable.Cell>+24</DataTable.Cell>
          <DataTable.Cell>{winddirection[8]}</DataTable.Cell>
</DataTable.Row>
</DataTable>
<Text style={styles.title2}>rain</Text>
<LineChart
    data={{
      labels: ['+0', '+3', '+6', '+9', '+12', '+15','+18','+21','+24'],
      datasets: [
        {
        data: rain,
        color:(opacity = 1) => `rgba(0, 255, 0,${opacity})`
        }
    ]
    }}
    width={Dimensions.get('window').width}
    height={220}
    chartConfig={{
      backgroundColor: '#e26a00',
      backgroundGradientFrom: '#fb8c00',
      backgroundGradientTo: '#ffa726',
      decimalPlaces: 2,
      color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
      style: {
        borderRadius: 16
      }
    }}
    
    style={{
      marginVertical: 8,
      borderRadius: 16
    }}
  />
<Text style={styles.title2}>snow</Text>
<LineChart
    data={{
      labels: ['+0', '+3', '+6', '+9', '+12', '+15','+18','+21','+24'],
      datasets: [
        {
        data: snow,
        color:(opacity = 1) => `rgba(0, 255, 0,${opacity})`
        }
    ]
    }}
    width={Dimensions.get('window').width}
    height={220}
    chartConfig={{
      backgroundColor: '#e26a00',
      backgroundGradientFrom: '#fb8c00',
      backgroundGradientTo: '#ffa726',
      decimalPlaces: 2,
      color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
      style: {
        borderRadius: 16
      }
    }}
    
    style={{
      marginVertical: 8,
      borderRadius: 16
    }}
  />
<Text style={styles.title2}>humidity</Text>
      <LineChart
    data={{
      labels: ['+0', '+3', '+6', '+9', '+12', '+15','+18','+21','+24'],
      datasets: [
        {
        data: humi,
        color:(opacity = 1) => `rgba(0, 255, 0,${opacity})`
        }
    ]
    }}
    width={Dimensions.get('window').width}
    height={220}
    chartConfig={{
      backgroundColor: '#e26a00',
      backgroundGradientFrom: '#fb8c00',
      backgroundGradientTo: '#ffa726',
      decimalPlaces: 2,
      color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
      style: {
        borderRadius: 16
      }
    }}
    
    style={{
      marginVertical: 8,
      borderRadius: 16
    }}
  />
  <Text style={styles.title2}>pressure</Text>
      <LineChart
    data={{
      labels: ['+0', '+3', '+6', '+9', '+12', '+15','+18','+21','+24'],
      datasets: [
        {
        data: pressure,
        color:(opacity = 1) => `rgba(0, 255, 0,${opacity})`
        }
    ]
    }}
    width={Dimensions.get('window').width}
    height={220}
    chartConfig={{
      backgroundColor: '#e26a00',
      backgroundGradientFrom: '#fb8c00',
      backgroundGradientTo: '#ffa726',
      decimalPlaces: 2,
      color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
      style: {
        borderRadius: 16
      }
    }}
    
    style={{
      marginVertical: 8,
      borderRadius: 16
    }}
  />
  </View>
  </ScrollView>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  table: {
    paddingHorizontal: 30,
  },
  title: {
    fontSize:win.width/10
  },
  title2: {
    fontSize:win.width/15
  },
  title3: {
    fontSize:win.width/20
  },
});


